#!/bin/bash

# ensure that you have a file /opt/cam/cam-ip.txt with only the camera IP
#

CAMIP=$(cat /opt/cam/cam-ip.txt)
CAMCRED=ajoiner:VMware1

if [ -z "$CAMIP" ]; then
	echo CAMIP not found
	exit -1
fi


/usr/bin/curl --trace-ascii /tmp/debugdump.txt --digest -u $CAMCRED -0 http://$CAMIP/axis-cgi/firmwaremanagement.cgi?method=rollback\&apiVersion=1.0

