import subprocess

# Improvements: 
# - read device in from file, or make it an env variable
# - is iotc-client the right one? Or DefaultClient?

PULSE_BIN='/opt/vmware/iotc-agent/bin/'

pulseDeviceID = '4d90ef98-5b9b-4bae-b03b-f5fffad404e9'


def send_properties(key, value):
    """Sends a single property for the current device

    Leverages the Pulse iot-client to exercise the Pulse SDK

     send-properties
        --device-id=<device Id> --key=<key> --value=<value>
    """
    value = value.replace(' ','-')
    print "new Value = "+value
    bashCommand = PULSE_BIN+"iotc-agent-cli send-properties --device-id="+pulseDeviceID+" --key="+key+" --value="+value
    print bashCommand


    print "Run the send-properties command"
    process = subprocess.check_call(bashCommand.split())


def send_metric(key, value, type="integer"):
    """Sends a single metric for the current device

    Leverages the Pulse iot-client to exercise the Pulse SDK

     send-metric
        --device-id=<device Id> --name=<metric name>
        --type=<string|integer|double|boolean> --value=<value>
        [ --device-id=<device2 Id> ... ]

    """
    bashCommand = PULSE_BIN+"iotc-agent-cli send-metric --device-id="+pulseDeviceID+" --name="+key+" --type="+type+" --value="+str(value)
    print bashCommand


    print "Run the send-metric command"
    process = subprocess.check_call(bashCommand.split())




