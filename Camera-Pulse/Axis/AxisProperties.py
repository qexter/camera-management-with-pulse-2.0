import time

from requests.auth import HTTPDigestAuth
import requests
import logging
from PulseAgentAPI import send_properties, send_metric
import xml.etree.ElementTree as ET  # for xml parsing


# V2.0 
# Improvements - 
# -  parameterize the api functions (for axis and hanwha)
# - 

# Camera Definitions - update for each camera
cameraName = "Cam-A-5F"
cameraURL = "http://10.84.140.39/"
cameraAuth = ('ajoiner', 'VMware1')
	

def processRequestforText(url):
    """Handles the setup and response of an HTTP request

    Specifically written to process Axis VAPIX web requests that return a list of values in
    the format name:value

    Pass in the full url
    Returns a dict of all the name value pairs received from the call
    returns None if nothing is retrieved or if the url could not be reached
    """


    try:
        resp = requests.get(url, auth=HTTPDigestAuth(*cameraAuth))

    except Exception as e:
        print "Exception while requesting http {0} - {1}".format(url, str(e))
#        log.error(
#            "Exception while requesting http {0} - {1}".format(url, str(e)))
        return None

#    print resp
#    log.debug("HTTP Request - response - {0}".format(resp))
    values = {}
    for line in resp.iter_lines():
        if line:
            name, var = line.partition("=")[::2]
            values[name.strip()] = var
#    log.debug("--processRequest")
    return values



def processRequestforXML(url):
    """Handles the setup and response of an HTTP request for XML return values (secret api calls)

    Specifically written to process a "hidden" call to retrieve Storage Disk data

    Pass in the full url
    Returns a dict of storage card data
    returns None if nothing is retrieved or if the url could not be reached
    """
#    log.info("++ProcessRequest - XML")

    try:
        resp = requests.get(url, auth=HTTPDigestAuth(*cameraAuth))
#        log.info("request complete")

    except Exception as e:
        print 'Exception while requesting http {0} - {1}'.format(url, str(e))
#        log.error(
#            'Exception while requesting http {0} - {1}'.format(url, str(e)))
        return None
    #
    # This is not a generic function - it only retrieves Storage Card data
    # the data retrieved from the call looks like:
    #
    #<root>
    #   <disks numberofdisks="1">
    #   <disk diskid="SD_DISK" name="" totalsize="15206688" freesize="3187816" cleanuplevel="0" cleanupmaxage="7"
    #       cleanuppolicy="fifo" locked="no"full="no" readonly="no" status="OK" filesystem="vfat" group="S0"
    #       requiredfilesystem="none" encryptionenabled="false"diskencrypted="false"/>
    #  </disks>
    #</root>
    #
    with open('AxisStorage.xml', 'wb') as f:
        f.write(resp.content)

#    log.info("wrote xml to file")

    tree = ET.parse('AxisStorage.xml')

#    log.info("parsed file into dict")
    values = {}
    root = tree.getroot()
    for item in root.findall('./disks/disk'):
        values = item.attrib

#    log.info("returning values")

    return values



def read_ParameterbyURL(cameraParameter, parameterURL):  # Axis
    """ request parameter "cameraPameter" via http request from a camera
    camerParameter must be the full group name of the parameter from Axis VAPIX api

    returns a value that matches the parameter name
        Accepts 
    """

    print "url = " + parameterURL
    deviceinfo = processRequestforText(parameterURL)
    cameraAttributeValue = "Not Available"   
    if deviceinfo:
        if cameraParameter in deviceinfo:
            cameraAttributeValue = deviceinfo[cameraParameter]
            if cameraAttributeValue == "":
                cameraAttributeValue = "Not Available"
        else:
            cameraAttributeValue = "Not Available"
    return cameraAttributeValue


def read_Parameter(cameraParameter):  # Axis
    return read_ParameterbyURL(cameraParameter, cameraURL  + "axis-cgi/param.cgi?action=list" + "&group=" + cameraParameter)



# Function to extract used storage space
def read_usedStorage():
#    log.info("++usedStorage")
    storage = {}

    # Get a metric here
    url = cameraURL + "axis-cgi/disks/list.cgi?diskid=SD_DISK"
    deviceinfo = processRequestforXML(url)

    if deviceinfo:
#        log.info("Got deviceinfo")
        for cameraAttribute in ["freesize"]:
#            log.info("Got freesize")
            if cameraAttribute in deviceinfo:
#                log.info("returning value")
                return int(deviceinfo[cameraAttribute])
#    log.info("--usedStorage - nothing returned")
    return None



def read_Network():
# Don't use this as a model.  It's just checking to see if the http call works,
# not actually using any value in it
    Network = {}

    # Get a metric here
    url = cameraURL + "axis-cgi/param.cgi?action=list" + "&group=Brand.Brand"
    Network = processRequestforText(url)
    if Network:
        return 1
    return 0


def read_NetworkTransmitted():
    Value = {}

    # Get a metric here
    Value = read_ParameterbyURL("Network Transfered Bytes", cameraURL  + "local/vmware/get.cgi?netstat")
    if int(Value) > 0:
#        return int(Value)//1024//1024 (should be in MB)
        return int(Value)  #but returning bytes for now

    return 0


def read_NetworkReceived():
    Value = {}

    # Get a metric here
    Value = read_ParameterbyURL("Network Revieved Bytes", cameraURL  + "local/vmware/get.cgi?netstat")
    if int(Value) > 0:
#        return int(Value)//1024//1024 (should be in MB)
        return int(Value)  #but returning bytes for now

    return 0

# Retrieve device properties

cameraAttribute = "Brand"

cameraValue = read_Parameter("root.Brand.Brand")
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "ModelNumber"
cameraValue = read_Parameter('root.Brand.ProdNbr')
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "HostName"
cameraValue = read_Parameter('root.Network.HostName')
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "IPAddress"
cameraValue = read_Parameter('root.Network.eth0.IPAddress')
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "MACAddress"
cameraValue = read_Parameter('root.Network.eth0.MACAddress')
cameraValue = cameraValue.replace(":","-")
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "IPV6Address"
cameraValue = read_Parameter('root.Network.eth0.IPv6.IPAddresses')
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "ONVIFVersion"
cameraValue = read_Parameter('root.Properties.API.WebService.ONVIF.Version')
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "FirmwareBuildDate"
cameraValue = read_Parameter('root.Properties.Firmware.BuildDate')
#log.info(cameraValue)
cameraValue = cameraValue.replace(":","-")
#log.info(cameraValue)
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "FirmwareVersion"
cameraValue = read_Parameter('root.Properties.Firmware.Version')
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))

cameraAttribute = "SerialNumber"
cameraValue = read_Parameter('root.Properties.System.SerialNumber')
print "set_property {0} = {1}".format(cameraAttribute, cameraValue)
send_properties(cameraAttribute, cameraValue)
#log.debug("set_property {0} = {1}".format(cameraAttribute, cameraValue))


while(1):
#   send_metric("PeopleCount", read_peopleCount(), "integer")
   send_metric("Online", read_Network(), "boolean")
#   send_metric("NetworkTransmitted", read_NetworkTransmitted(), "integer")
#   send_metric("NetworkReceived", read_NetworkReceived(), "integer")
   time.sleep(300)

