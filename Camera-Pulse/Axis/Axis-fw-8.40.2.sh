#!/bin/sh
DIRNAME=$(echo `echo $(dirname "$0")`)
cd $DIRNAME

CAMIP=$(cat /opt/cam/cam-ip.txt)

FILENAME=Q3505_Mk_II_8_40_2.bin
CAMCRED=ajoiner:VMware1

echo "This is the script: execute" >> /tmp/campaign.log
echo "Execution location: $DIRNAME" >> /tmp/campaign.log
echo "Purpose: Update Firmware on Axis Camera" >> /tmp/campaign.log

pwd >> /tmp/campaign.log 

echo "filename=@./$FILENAME;type=application/octet-stream" >> /tmp/campaign.log

curl -H "Content-Type:multipart/form-data" -F "filename=@./$FILENAME;type=application/octet-stream" --digest -u "$CAMCRED" -0 "http://$CAMIP/axis-cgi/firmwareupgrade.cgi?type=normal" &


echo "made firmware update request $FILENAME $CAMCRED $CAMIP" >> /tmp/campaign.log
